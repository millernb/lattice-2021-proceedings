import gvar as gv
import matplotlib.pyplot as plt
import os

def min_max(arr):
    pm = lambda g, k : gv.mean(g) + k *gv.sdev(g)

    arr_min = pm(arr[0], -1)
    arr_max = pm(arr[0], +1)
    for g in arr:
        if pm(g, -1) < arr_min:
            arr_min = pm(g, -1) 
        if pm(g, +1) > arr_max:
            arr_max = pm(g, +1) 

    return arr_min, arr_max

project_path = os.path.normpath(os.path.join(os.path.realpath(__file__), os.pardir, os.pardir))

plt.rcParams["figure.figsize"] = (10,3)
plt.rcParams["font.size"] = 16
plt.rcParams["text.usetex"] = True

print(gv.gvar('0.2236(15)')/gv.gvar('1.1932(19)') * gv.gvar('1.1942(45)'))

v_us = {
    r'$\tau$ (semi-incl.)' : gv.gvar('0.2195(19)'),
    #r'$\tau$ (inclusive)' : gv.gvar('0.2231(27)'),
    'Hyperons (pheno.)' : gv.gvar('0.2250(27)'),
    r'$K_{\ell 2} \enspace \& \enspace F_K/F_\pi$' : gv.gvar('0.2252(05)'),
    r'$K_{\ell 3} \enspace \& \enspace f^+(0)$' : gv.gvar('0.2232(06)'),
}

fig, ax = plt.subplots()

colors = ['lightcoral', 'royalblue', 'seagreen', 'seagreen']
#colors = ['seagreen', 'seagreen','lightcoral', 'royalblue', 'royalblue']
#colors = ['royalblue', 'royalblue','lightcoral', 'seagreen', 'seagreen']
pm = lambda g, k : gv.mean(g) + k *gv.sdev(g)
for j, key in enumerate(v_us):
    color = colors[j]
    ax.errorbar(x=gv.mean(v_us[key]), xerr=gv.sdev(v_us[key]), y=j, yerr=0, fmt='o', mec='white', color=color, lw=10, ms=15)

j = 0  
for arr in [
        [v_us[r'$\tau$ (semi-incl.)']],#, v_us[r'$\tau$ (inclusive)']], 
        [v_us['Hyperons (pheno.)']],
        [v_us[r'$K_{\ell 2} \enspace \& \enspace F_K/F_\pi$'], v_us[r'$K_{\ell 3} \enspace \& \enspace f^+(0)$']]
    ]:
    xmin, xmax = min_max(arr)
    color = colors[j]
    ax.axvspan(xmin=xmin, xmax=xmax, color=color, alpha=0.5)
    j += len(arr)

plt.yticks(range(len(v_us)), list(v_us))
plt.xlabel(r'$|V_{us}|$')
plt.ylim(-0.5, len(v_us)-0.5)
plt.tight_layout()
plt.savefig(f'{project_path}/figs/vus_sources.pdf', transparent=True)
#plt.show()